//Redux imports
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
//Reducer import FIXME: need to import root reducer from seperate file
function rootReducer( state = []) {
	return state;
};

const defaultState = {
	zipCode: "",
	currentReportType: "",
	dailyReport: {
		maxTemp: "",
		minTemp: "",
		morningPrecip: "",
		eveningPrecip: "",
		morningConditions: "",
		eveningConditions: ""
	},
	weeklyReport: {
		maxTemp: [],
		minTemp: [],
		morningPrecip: [],
		eveningPrecip: [],
		morningConditions: [],
		eveningConditions: []
	},
}

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
	rootReducer,
	defaultState,
	composeEnhancer(applyMiddleware(thunk)),
);