//React imports
import React from 'react';
import ReactDOM from 'react-dom';

//Redux imports
import { store } from './redux/store';
import { Provider } from 'react-redux';

//Component imports
import App from './components/app';

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>, 
	document.getElementById('root')
);

