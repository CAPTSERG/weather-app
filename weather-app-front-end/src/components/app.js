//React imports
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

//Axios import
import axios from 'axios';

//Semantic UI imports
import { Menu, Segment, Header } from 'semantic-ui-react';

//Component imports
import WeatherReport from './weather-report/weatherReport';
import DailyWeather from './daily-weather/dailyWeather';
import WeeklyWeather from './weekly-weather/weeklyWeather';

class App extends Component {

	state = {
		helloMessage: [],
		activeItem: "Home"
	};

	render () {
		const { helloMessage, activeItem } = this.state;
		const { handleMenuClick } = this;

		return (
			<React.Fragment>
				<Router>
					<Segment inverted color="olive">
						<Header as="h2" content="Your Weather v1.0" />
						<Menu inverted color="grey">
							<Menu.Item 
								name="Home"
								active={activeItem === "Home"}
								onClick={handleMenuClick}
								as={Link}
								to="/"
							/>
							<Menu.Item 
								name="Daily"
								active={activeItem === "Daily"}
								onClick={handleMenuClick}
								as={Link}
								to="/daily"
							/>
							<Menu.Item 
								name="Weekly"
								active={activeItem === "Weekly"}
								onClick={handleMenuClick}
								as={Link}
								to="/weekly"
							/>
						</Menu>
					</Segment>

					<Route path="/" exact render={ (props) => <WeatherReport {...props} />} />
					<Route path="/daily" exact render={ (props) => <DailyWeather {...props} />} />
					<Route path="/weekly" exact render={ (props) => <WeeklyWeather {...props} />} />

				</Router>
				<h3>Response from Server: </h3><br/>
				<p>Max Temps for the week: {helloMessage.maxTemp}</p>
				<p>Min Temps for the week: {helloMessage.minTemp}</p>
				<p>Morning Precipitation chances for the week: {helloMessage.morningPrecip}</p>
				<p>Evening Precipitation chances for the week: {helloMessage.eveningPrecip}</p>
				<p>Morning Conditions for the week: {helloMessage.morningConditions}</p>
				<p>Evening Conditions for the week: {helloMessage.eveningConditions}</p>
			</React.Fragment>
		);
	};

	//Menu Item Click Event Handler
	handleMenuClick = (event, {name}) => {
		this.setState({
			...this.state,
			activeItem: name
		});
	}

	//Whenever Component is rendered
	componentDidMount() {
		console.log("Sending get request")
		let testObject = {
			zipCode: "78006",
			startDate: "2019-07-05",
			numDays: "7"
		}

		const { zipCode, startDate, numDays } = testObject;

		let urlString = `http://localhost:4000/get/WeatherReport/${zipCode}/${startDate}/${numDays}`;
		setTimeout( () => {
			axios.get(urlString)
			.then( res => {
				console.log(res);
				this.setState({
					helloMessage: res.data
				});
			})
			.catch( err => {
				console.log(err);
			})
		}, 
		5000);	
	}
};



export default App;