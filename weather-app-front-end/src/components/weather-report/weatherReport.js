//React imports
import React, { Component } from 'react';

//Componenet imports
import WeatherReportForm from './weatherReport-form';

class WeatherReport extends Component {

	state = {
		reportType: "",
		zipCode: "",
		reportTypeOptions: [
			{text: "Daily", value: "Daily"},
			{text: "Weekly", value: "Weekly"}
		]
	};

	render() {

		const { handleDropdownChange, handleInputChange, onSubmit } = this;

		return(
			<React.Fragment>
				<WeatherReportForm 
					onSubmit={onSubmit} 
					handleDropdownChange={handleDropdownChange} 
					handleInputChange={handleInputChange} 
					inputFields={this.state}
				/>
			</React.Fragment>
		);
	};

	handleDropdownChange = (event, { name, value }) => {
		this.setState({
			...this.state,
			[name]: value
		});
		console.log(`Changed ${name} to: ${value}`);
	};

	handleInputChange = event => {
		const { name, value } = event.target;
		this.setState({
			...this.state,
			[name]: value
		});
		console.log(`Changed ${name} to: ${value}`);
	};

	onSubmit = () => {

		const { reportType, zipCode } = this.state;

		const submitObject = {
			reportType: reportType,
			zipCode: zipCode
		};

		console.log("Submiting WeatherReportForm: ", submitObject);

		//FIXME: Action call here to generateWeatherReport
		// onSubmit(submitObject);
		this.setState({
			...this.state,
			reportType: "",
			zipCode: "",
		});
	};
};

export default WeatherReport;