//React imports
import React, { Component } from 'react';

//Semantic UI imports
import { Form, Icon, Header, Segment, Dropdown, Divider, Button, Popup } from 'semantic-ui-react';

class WeatherReportForm extends Component {

	render() {

		const { reportType, zipCode, reportTypeOptions } = this.props.inputFields;
		const { handleDropdownChange, handleInputChange, onSubmit } = this.props;

		return(
			<React.Fragment>
				<Popup trigger={
					<Button icon size="mini" color="teal" labelPosition="left">
						<Icon name="plus" />
						Generate Weather Report
					</Button>
					}
					on="click"
					position="right center"
				>			
					<Header as="h2">
						<Icon name="cloud" />
						<Header.Content>Generate A Weather Report</Header.Content>
					</Header>
					<Divider />
					<Segment raised>
						<Form size="small" onSubmit={onSubmit}>

							<Form.Field>
								<label>Report Type</label>
								<Dropdown 
									selection 
									name="reportType" 
									options={reportTypeOptions} 
									placeholder="Report Type"
									value={reportType}
									onChange={handleDropdownChange}
								/> 

								<label>Zip Code</label>
								<input 
									name="zipCode" 
									placeholder="Please enter your zip code"
									value={zipCode}
									onChange={handleInputChange}
								 />

							</Form.Field>

							<Button color="green" size="mini" floated="right" content="Create Event" />

						</Form>
					</Segment>
				</Popup>
			</React.Fragment>
		);
	};

};

export default WeatherReportForm;