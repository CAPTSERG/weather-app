//Exports setup
var exports = module.exports = {}

exports.setEnvelopeParams = ( req, latLon ) => {

   const { startDate, numDays } = req.params;
   const lat = latLon[0];
   const long = latLon[1];

 return NDFDgenByDay_Daily_envelope = `
   <soapenv:Envelope xmlns:xsi="http://www.w3org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ndf="https://graphical.weather.gov/xml/DWMLgen/wsdl/ndfdXML.wsdl">
      <soapenv:Header/>
      <soapenv:Body>
         <ndf:NDFDgenByDay soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
            <latitude xsi:type="xsd:decimal">${lat}</latitude>
            <longitude xsi:type="xsd:decimal">${long}</longitude>
            <startDate xsi:type="xsd:date">${startDate}</startDate>
            <numDays xsi:type="xsd:integer">${numDays}</numDays>
            <Unit xsi:type="xsd:string">e</Unit>
            <format xsi:type="xsd:string">12 hourly</format>
         </ndf:NDFDgenByDay>
      </soapenv:Body>
   </soapenv:Envelope>`;
};