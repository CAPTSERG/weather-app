//Soap Request imports
const soapRequest = require('easy-soap-request');
const { parseString } = require('xml2js');

//Exports setup
var exports = module.exports = {}

//Envelope import
const envelope = require("../request-envelopes/NDFDgenByDay_envelope");
const { setEnvelopeParams } = envelope;

//Lat and Long import
var requestContainer = require('./requests-container');
const { getLatLonByZip } = requestContainer;

exports.getNDFDgenByDay = async ( req, res ) => {
	try{

		//Request setup
		latLon = await getLatLonByZip( req );
		const url = "https://graphical.weather.gov/xml/SOAP_server/ndfdXMLserver.php";
		const xml = setEnvelopeParams( req, latLon );
		const headers = {
			'Content-Type': 'text/xml;charset=UTF-8',
			'soapAction': "https://graphical.weather.gov/xml/DWMLgen/wsdl/ndfdXML.wsdl#NDFDgenByDay"
		};

		//Request
		const { response } = await soapRequest(url, headers, xml, 10000);
		const { body, statusCode } = response;

		// Format response and send to front-end
		let weatherReport = weekTrim(body);
		console.log( "StatusCode: ", statusCode );
		console.log( "Successfully received Weather Report from SOAP server: ", weatherReport );
		res.status( statusCode ).send( weatherReport );

	} catch (err) {
		console.log(err)
	}
};

//Converts xml to needed Day Weather Object
dayTrim = body => {
	let responseObject = {
		maxTemp: "",
		minTemp: "",
		morningPrecip: "",
		eveningPrecip: "",
		morningConditions: "",
		eveningConditions: ""
	};
	
	parseString(body, { explicitArray : false, ignoreAttrs : true, }, ( err, result ) => {
		let xmlString = result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns1:NDFDgenByDayResponse"].dwmlByDayOut;
		
		//Temp variables used to parse for needed values
		let tempString = "";
		let indexStart = 0;
		let indexEnd = 0;

		//Populate responseObject 
		for (let i = 0; i < 6; ++i){

			if ( i < 4 ){
				xmlString = xmlString.slice( indexEnd + 5 );
				indexStart = xmlString.indexOf("<value>") + 7;
				indexEnd = xmlString.indexOf("</value>");
				tempString = xmlString.slice( indexStart, indexEnd );
			} else {
				xmlString = xmlString.slice( indexEnd + 5 );
				indexStart = xmlString.indexOf("summary") + 9;		
				indexEnd = xmlString.indexOf("/>") - 1;
				tempString = xmlString.slice( indexStart, indexEnd );
			}

			switch (i) {
				case 0:
					responseObject.maxTemp = tempString;
					break;
				case 1:
					responseObject.minTemp = tempString;
					break;
				case 2:
					responseObject.morningPrecip = tempString;
					break;
				case 3:
					responseObject.eveningPrecip = tempString;
					break;
				case 4:
					responseObject.morningConditions = tempString;
					break;
				case 5:
					responseObject.eveningConditions = tempString;
					break;
			};
		}
	});

	return responseObject;
};


//Converts xml to needed Week Weather Object
weekTrim = body => {
	let responseObject = {
		//Array of Seven values pertaining to the Seven Days
		maxTemp: [],
		minTemp: [],
		morningPrecip: [],
		eveningPrecip: [],
		morningConditions: [],
		eveningConditions: []
	};
	
	parseString(body, { explicitArray : false, ignoreAttrs : true, }, ( err, result ) => {
		let xmlString = result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns1:NDFDgenByDayResponse"].dwmlByDayOut;
		
		console.dir(xmlString);

		//Temp variables used to parse for needed values
		let tempString = "";
		let indexStart = 0;
		let indexEnd = 0;

		//Populate responseObject maxTemp
		for (let i = 0; i < 7; ++i){

			xmlString = xmlString.slice( indexEnd + 5 );
			indexStart = xmlString.indexOf("<value>") + 7;
			indexEnd = xmlString.indexOf("</value>");
			tempString = xmlString.slice( indexStart, indexEnd );

			//Push to maxTemp array
			responseObject.maxTemp.push(tempString);	
		}

		//Populate responseObject minTemp
		for (let i = 0; i < 6; ++i){

			xmlString = xmlString.slice( indexEnd + 5 );
			indexStart = xmlString.indexOf("<value>") + 7;
			indexEnd = xmlString.indexOf("</value>");
			tempString = xmlString.slice( indexStart, indexEnd );

			//Push to minTemp array
			responseObject.minTemp.push(tempString);	
		}

		//Populate responseObject morning/evening Precip
		for (let i = 0; i < 6; ++i){
			
			//Get morningPrecip value
			xmlString = xmlString.slice( indexEnd + 5 );
			indexStart = xmlString.indexOf("<value>") + 7;
			indexEnd = xmlString.indexOf("</value>");
			tempString = xmlString.slice( indexStart, indexEnd );

			//Push to morningPrecip array
			responseObject.morningPrecip.push(tempString);

			//Get eveningPrecip value
			xmlString = xmlString.slice( indexEnd + 5 );
			indexStart = xmlString.indexOf("<value>") + 7;
			indexEnd = xmlString.indexOf("</value>");
			tempString = xmlString.slice( indexStart, indexEnd );

			//Push to eveningPrecip array
			responseObject.eveningPrecip.push(tempString);	
		}

		//Final morning precip value
		xmlString = xmlString.slice( indexEnd + 5 );
		indexStart = xmlString.indexOf("<value>") + 7;
		indexEnd = xmlString.indexOf("</value>");
		tempString = xmlString.slice( indexStart, indexEnd );

		//Push to morningPrecip array
		responseObject.morningPrecip.push(tempString);

		//Populate responseObject morning/evening Conditions
		for (let i = 0; i < 6; ++i){
			
			//Get morningConditions value
			xmlString = xmlString.slice( indexEnd + 5 );
			indexStart = xmlString.indexOf( "summary" ) + 9;	
			xmlString = xmlString.slice( indexStart );	
			indexEnd = xmlString.indexOf( "/>" ) - 1;
			tempString = xmlString.slice( 0, indexEnd );

			//Push to morningConditions array
			responseObject.morningConditions.push(tempString);

			//Get eveningConditions value
			xmlString = xmlString.slice( indexEnd + 5 );
			indexStart = xmlString.indexOf("summary") + 9;		
			indexEnd = xmlString.indexOf("/>") - 1;
			tempString = xmlString.slice( indexStart, indexEnd );

			//Push to eveningConditions array
			responseObject.eveningConditions.push(tempString);	
		}

		//Final morning condition value
		xmlString = xmlString.slice( indexEnd + 5 );
		indexStart = xmlString.indexOf("summary") + 9;		
		indexEnd = xmlString.indexOf("/>") - 1;
		tempString = xmlString.slice( indexStart, indexEnd );

		//Push to morningConditions array
		responseObject.morningConditions.push(tempString);
		


	});

	return responseObject;
};