//Export setup
var exports = module.exports = {};

//Exports
exports.getLatLonByZip = require('./getLatLonByZip').getLatLonByZip;
exports.getNDFDgenByDay = require('./getNDFDgenByDay').getNDFDgenByDay;
