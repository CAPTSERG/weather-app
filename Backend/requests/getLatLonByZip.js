//Soap Request imports
const soapRequest = require('easy-soap-request');
const { parseString } = require('xml2js');

//Exports setup
var exports = module.exports = {}

//Envelope import
const envelope = require("../request-envelopes/LatLonListZipCode_envelope");
const { setEnvelopeParams } = envelope;

exports.getLatLonByZip = async ( req ) => {

	try{
		//Request setup
		const url = "https://graphical.weather.gov/xml/SOAP_server/ndfdXMLserver.php";
		const xml = setEnvelopeParams( req );
		const headers = {
			'Content-Type': 'text/xml;charset=UTF-8',
			'soapAction': "https://graphical.weather.gov/xml/DWMLgen/wsdl/ndfdXML.wsdl#LatLonListZipCode"
		};

		//Request
		const { response } = await soapRequest(url, headers, xml, 10000);
		const { body, statusCode } = response;

		//Format response and send to getNDFDgenByDay request
		let latLon = latLonTrim(body);
		console.log("StatusCode: ", statusCode);
		console.log("Successfully recieved Lat/Lon pair from SOAP server: ", latLon);
		return latLon;

	} catch (err) {
		console.log(err)
	}
};

//Converts xml to needed Lat/Lon Array
latLonTrim = body => {
	let responseArray = [];
	let responseString = "";
	parseString(body, { explicitArray : false, ignoreAttrs : true, }, ( err, result ) => {
		responseString = result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns1:LatLonListZipCodeResponse"].listLatLonOut;
		let indexOfLatStart = responseString.indexOf("<latLonList") + 12;
		let indexOfLatEnd = responseString.indexOf("</latLonList");
		responseString = responseString.slice( indexOfLatStart, indexOfLatEnd );
		
		//Push Lat into response Array
		responseArray.push( responseString.slice( 0, responseString.indexOf(",") ) );

		//Push Long into response Array
		responseArray.push( responseString.slice( (responseString.indexOf(",") + 1), (responseString.length -1) ) );

	})
	return responseArray
};