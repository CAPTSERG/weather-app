//Express Setup
var express = require('express');
var app = express();

//Body Parser setup
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//Cors setup
const cors = require("cors");
app.use( ( req, res, next ) => {
	res.header("Access-Control-Allow-Origin", "*");
  	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  	next();
});

//Start the express server
var server = app.listen(4000, function () {
var port = server.address().port;
  console.log(`Server Listening at Port : ${port}`);
});

//Requests imports
var requestContainer = require('./requests/requests-container');
const { getNDFDgenByDay } = requestContainer;


//Cleaner version
app.get("/get/WeatherReport/:zipCode/:startDate/:numDays", async ( req, res ) => {
	console.log("Here are your url params", req.parmas);
	getNDFDgenByDay( req, res );
});


